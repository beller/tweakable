Make interactive and generative music, sound and visual art.

Choose from an extensive library of pre-built sequencing, logic, audio, video and effects objects. Connect them together to create an algorithmic system. Then build a user interface so your algorithm can be tweaked.

Share your artworks with others around the world, on mobile and desktop devices.

Allow form to evolve and experiencers to interact, dissolving the barrier between artwork and experiencer, composer and listener.

Invent your own rules
An algorithm can suggest new ways of thinking about art and music and provide liberating constraints. Think of it as a rule system that shapes the arc of a composition. Form emerges in response, as the positions of leaves follow the structure of a tree.

[See examples
](https://tweakable.org/examples)